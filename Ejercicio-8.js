/*   CREACION DE LA COLECCION 8    */

db.getCollection("registro_cliente").insertMany([
                                            {
                                                "Nombres": "Pablo",
                                                "Apellidos": "Fernandez Aduviri",
                                                "Edad": 18,
                                                "Correo": "pablo.juegos12345@gmail.com"
                                             },
                                             {
                                                 "Nombres": "Alfredo",
                                                 "Apellidos": "Nogales Garcia",
                                                 "Edad": 21,
                                                 "Correo": "alfredo.nogales@hotmail.com"
                                             },
                                             {
                                                 "Nombres": "Malenna",
                                                 "Apellidos": "Torrez Mendez",
                                                 "Edad": 18,
                                                 "Correo": "mtorrez@gmail.com"
                                              },
                                             {
                                                 "Nombres": "Rosa",
                                                 "Apellidos": "Valencia Del Prado",
                                                 "Edad": 22,
                                                 "Correo": "rvalencia@gmail.com"
                                             },
                                             {
                                                 "Nombres": "Juan Pablo",
                                                 "Apellidos": "Lopez Guevara",
                                                 "Edad": 21,
                                                 "Correo": "jplopez@gmail.com"
                                             },
                                             {
                                                 "Nombres": "Lucia",
                                                 "Apellidos": "Camacho Salinas",
                                                 "Edad": 18,
                                                 "Correo": "lcamacho@gmail.com"
                                             },
                                             {
                                                 "Nombres": "Julia",
                                                 "Apellidos": "Mancilla Chavez",
                                                 "Edad": 22,
                                                 "Correo": "julia123@gmail.com"
                                             },
                                             {
                                                 "Nombres": "Jose",
                                                 "Apellidos": "Ortu�o Lopez",
                                                 "Edad": 18,
                                                 "Correo": "josejose@gmail.com"
                                             },
                                       ])
                                             
db.getCollection("registro_cliente").find({"Apellidos":"Fernandez Aduviri"})

                                              
/*      CREACION DE LOS INDICES DE BUSQUEDA OPTIMIZADA       */
db.getCollection("registro_cliente").createIndex({"Apellidos": 1})
db.getCollection("registro_cliente").createIndex({"Edad": 1})
db.getCollection("registro_cliente").createIndex({"Correo": 1},{unique: true})

db.getCollection("registro_cliente").getIndexes()


/*    CONSULTAS PARA LA COLECCION    */

db.getCollection('registro_cliente').find({})

db.getCollection("registro_cliente").find({"Edad":18 })

db.getCollection('registro_cliente').find({})
